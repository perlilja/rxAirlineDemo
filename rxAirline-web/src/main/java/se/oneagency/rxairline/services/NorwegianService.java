package se.oneagency.rxairline.services;

import io.reactivex.Observable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import se.oneagency.rxairline.dto.DestinationDTO;
import se.oneagency.rxairline.dto.FlightDTO;

import java.util.Arrays;
import java.util.List;

@Service
public class NorwegianService extends ExternalAirlineService {

    @Override
    public Observable<DestinationDTO> getDestinations(String from) {
        Observable<DestinationDTO> result = Observable.fromArray(restTemplate.getForObject(encodeURI("http://localhost:10004/norwegian/api/destinations/{from}", from),DestinationDTO[].class));
        return result;
    }

    @Override
    public Observable<FlightDTO> getFlights(String from, String to) {

        try {
            FlightDTO[] resultArray = restTemplate.getForObject(encodeURI("http://localhost:10004/norwegian/api/flights/{from}/{to}",from,to),FlightDTO[].class);
            return Observable.fromArray(resultArray);
        } catch (HttpClientErrorException rce) {
        }


        return Observable.empty();
    }

}
