package se.oneagency.rxairline.services;


import io.reactivex.Observable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import se.oneagency.rxairline.dto.DestinationDTO;
import se.oneagency.rxairline.dto.FlightDTO;

@Service
public class EasyJetService extends ExternalAirlineService {

    @Override
    public Observable<DestinationDTO> getDestinations(String from) {
        Observable<DestinationDTO> result = Observable.fromArray(restTemplate.getForObject(encodeURI("http://localhost:10002/easyjet/api/destinations/{from}", from),DestinationDTO[].class));
        return result;
    }

    @Override
    public Observable<FlightDTO> getFlights(String from, String to) {

        try {
            FlightDTO[] resultArray = restTemplate.getForObject(encodeURI("http://localhost:10002/easyjet/api/flights/{from}/{to}",from,to),FlightDTO[].class);
            return Observable.fromArray(resultArray);
        } catch (HttpClientErrorException rce) {
        }


        return Observable.empty();
    }
}

