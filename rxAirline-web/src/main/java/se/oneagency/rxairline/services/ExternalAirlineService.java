package se.oneagency.rxairline.services;

import io.reactivex.Observable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;
import se.oneagency.rxairline.dto.DestinationDTO;
import se.oneagency.rxairline.dto.FlightDTO;

import java.net.URI;

public abstract class ExternalAirlineService {

    @Autowired
    protected RestTemplate restTemplate;

    public abstract Observable<DestinationDTO> getDestinations(String from);
    public abstract Observable<FlightDTO> getFlights(String from, String to);


    protected URI encodeURI(String url, String... params) {
        UriComponents uriComponents =
                UriComponentsBuilder.fromUriString(url).build().expand(params).encode();

        URI uri = uriComponents.toUri();

        return uri;
    }
}
