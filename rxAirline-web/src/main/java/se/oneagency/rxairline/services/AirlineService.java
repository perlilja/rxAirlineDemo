package se.oneagency.rxairline.services;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se.oneagency.rxairline.dto.FlightDTO;

import java.util.*;


@Service
public class AirlineService {

    @Autowired
    private ExternalAirlineService easyJetService;

    @Autowired
    private ExternalAirlineService ryanairService;

    @Autowired
    private ExternalAirlineService norwegianService;

    public Observable<FlightDTO> getPossibleDestinations (String from) {
        return Observable.merge(addEasyJetFlights(from),
                addRyanairFlights(from),
                addNorwegianFlights(from)).subscribeOn(Schedulers.io()).sorted(Comparator.comparing(o -> Integer.valueOf(o.getPrice())));
    }

    private Observable<FlightDTO> addEasyJetFlights(String from) {
        return easyJetService.getDestinations(from)
                .subscribeOn(Schedulers.io())
                .flatMap(dest -> easyJetService.getFlights(from, dest.getTo()))
                .map(f -> new FlightDTO(f.getFrom(), f.getDestination(), f.getDeparture(), f.getArrival(), f.getPrice(), "EasyJet"));
    }

    private Observable<FlightDTO> addRyanairFlights(String from) {
        return ryanairService.getDestinations(from)
                .subscribeOn(Schedulers.io())
                .flatMap(dest -> ryanairService.getFlights(from, dest.getTo()))
                .map(f -> new FlightDTO(f.getFrom(), f.getDestination(), f.getDeparture(), f.getArrival(), f.getPrice(), "Ryanair"));
    }

    private Observable<FlightDTO> addNorwegianFlights(String from) {
        return norwegianService.getDestinations(from)
                .subscribeOn(Schedulers.io())
                .flatMap(dest -> norwegianService.getFlights(from, dest.getTo()))
                .map(f -> new FlightDTO(f.getFrom(), f.getDestination(), f.getDeparture(), f.getArrival(), f.getPrice(), "Norwegian"));
    }

}
