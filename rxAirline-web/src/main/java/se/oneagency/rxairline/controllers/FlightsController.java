package se.oneagency.rxairline.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;
import se.oneagency.rxairline.dto.FlightDTO;
import se.oneagency.rxairline.services.AirlineService;

import java.util.List;

@RestController
public class FlightsController {

    @Autowired
    private AirlineService airlineService;


    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(value = "/flights/search/{from}", method = RequestMethod.GET)
    public SseEmitter searchFlights(@PathVariable String from) {
        SseEmitter sseEmitter = new SseEmitter();
        airlineService.getPossibleDestinations(from).subscribe(a -> {
            sseEmitter.send(a);
        },sseEmitter::completeWithError,sseEmitter::complete);

        return sseEmitter;
    }


}
