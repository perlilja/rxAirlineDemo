import {Injectable, Inject, NgZone} from '@angular/core';
import { Http } from '@angular/http';
import { Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';

declare let EventSource: any;

@Injectable()
export class FlightsService {

  private url: string = 'http://localhost:8080/flights/search/';


  constructor(private http: Http, private zone: NgZone) {
  }

  getFlights(from: string): Observable<any> {
    console.log("Calling");
    let observable =  Observable.create(observer => {
      let eventSource = new EventSource(this.url + from);

      eventSource.onmessage = x => {
        this.zone.run(() => observer.next(x.data));
      };

      eventSource.onerror = e => {
        if (e.eventPhase == EventSource.CLOSED) {
          observer.complete();
        }
      };
    });
    return observable;
  }
}
