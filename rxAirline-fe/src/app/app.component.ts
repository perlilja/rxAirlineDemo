import { Component } from '@angular/core';
import { FlightsService} from './flights.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [FlightsService]
})
export class AppComponent {

  flights = [];
  search: string;

  constructor(private flightsService: FlightsService) {
  }

  performSearch(from: string) {
    this.flightsService.getFlights(from)
      .subscribe(
      (data) => {
        this.flights.push(JSON.parse(data));
      }
    );
  }
}
