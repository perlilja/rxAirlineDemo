export interface IFlight {
  from: string;
  destination: string;
  departure: string;
  arrival: string;
  price: string;
  airline: string;
}
