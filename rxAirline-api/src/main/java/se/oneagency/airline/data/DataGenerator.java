package se.oneagency.airline.data;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import se.oneagency.airline.domain.Airport;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DataGenerator {

    private final static List<Airport> airports = new ArrayList<>();
    private final static String[] airlines = new String[]{"easyjet","ryanair","sas","norwegian"};

    static {

        byte[] jsonData = new byte[0];
        try {
            File file = new File(DataGenerator.class.getClassLoader().getResource("airports.json").getFile());
            jsonData = Files.readAllBytes(file.toPath());
            ObjectMapper objectMapper = new ObjectMapper();
            airports.addAll(objectMapper.readValue(jsonData, new TypeReference<List<Airport>>(){}));
            airports.removeIf(a -> a.getName() == null);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static List<String> getDestinationsFor(String airline) {
        Collections.shuffle(airports);
        return airports.subList(0,10).stream().map(a -> a.getName()).collect(Collectors.toList());
    }

    public static String generateRandomTime(boolean firstHalfOfDay) {
        String hourStr = "";
        int hour = 0;
        String minStr =  (int)(Math.random() * 6) + "0";
        if (firstHalfOfDay) {
            hour = (int)(Math.random() * 12);
        } else {
            hour = 12 + (int)(Math.random() * 12);
        }

        hourStr = hour < 10 ? "0" + hour : "" + hour;

        return hourStr + ":" + minStr;
    }

}
