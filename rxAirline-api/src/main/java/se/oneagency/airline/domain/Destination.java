package se.oneagency.airline.domain;


public class Destination {

    private final String from;
    private final String to;

    public Destination(String from, String to) {
        this.from = from;
        this.to = to;
    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }
}
