package se.oneagency.airline.domain;

import java.util.Date;

public class Flight {
    private String from;
    private String destination;
    private String departure;
    private String arrival;
    private String price;

    public Flight(String from, String destination, String departure, String arrival, String price) {
        this.from = from;
        this.destination = destination;
        this.departure = departure;
        this.arrival = arrival;
        this.price = price;
    }

    public String getFrom() {
        return from;
    }

    public String getDestination() {
        return destination;
    }

    public String getDeparture() {
        return departure;
    }

    public String getArrival() {
        return arrival;
    }

    public String getPrice() {
        return price;
    }
}
