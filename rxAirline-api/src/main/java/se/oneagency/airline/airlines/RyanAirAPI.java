package se.oneagency.airline.airlines;


import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import se.oneagency.airline.data.DataGenerator;
import se.oneagency.airline.domain.Destination;
import se.oneagency.airline.domain.Flight;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@SpringBootApplication
@RestController
@RequestMapping("/ryanair/api")
public class RyanAirAPI {

    @RequestMapping(value = "destinations/{from}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody List<Destination> destinations(@PathVariable String from) {
        return DataGenerator.getDestinationsFor("ryanair").stream().map(dest -> new Destination(from,dest)).collect(Collectors.toList());
    }

    @RequestMapping(value = "flights/{from}/{to}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody List<Flight> flights(@PathVariable String from, @PathVariable String to) {
        return IntStream.rangeClosed(0,3).boxed()
                .map(i -> new Flight(from,to, DataGenerator.generateRandomTime(true), DataGenerator.generateRandomTime(false), String.valueOf((int)(Math.random() * 1000))))
                .collect(Collectors.toList());
    }
}
