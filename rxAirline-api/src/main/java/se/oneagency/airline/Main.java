package se.oneagency.airline;


import org.springframework.boot.builder.SpringApplicationBuilder;
import se.oneagency.airline.airlines.EasyJetAPI;
import se.oneagency.airline.airlines.NorwegianAPI;
import se.oneagency.airline.airlines.RyanAirAPI;
import se.oneagency.airline.airlines.SASAPI;

public class Main {

    public static void main(String[] args) {
        new SpringApplicationBuilder(EasyJetAPI.class)
                .properties("server.port=${easyjet.port}").run();

        new SpringApplicationBuilder(RyanAirAPI.class)
                .properties("server.port=${ryanair.port}").run();

        new SpringApplicationBuilder(SASAPI.class)
                .properties("server.port=${sas.port}").run();

        new SpringApplicationBuilder(NorwegianAPI.class)
                .properties("server.port=${norwegian.port}").run();
    }

}
