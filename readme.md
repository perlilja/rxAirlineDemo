# How ro tun this application


This application consist of 3 parts

## rxAirline-api 

Are the so called 3:rd party APIs and is started by running it as a Spring Boot application.

_mvn spring-boot:run_

It will start three different APIs on port 10001, 10002 and 10003.

You can test it with the following url:

http://localhost:10001/ryanair/api/destinations/cph

http://localhost:10002/easyjet/api/destinations/cph

http://localhost:10003/norwegian/api/destinations/cph

## rxAirline-web 

This is the backend of the application, and is a started by using the Spring Boot plugin in maven.

_mvn spring-boot:run_

It will start at localhost and port 8080.

You can test it with the following url:

http://localhost:8080/flights/search/cph

## rxAirline-fe

The client is build with Angular 4, and needs node and npm installed to work.
Then you just need to run 

_npm install_

After that you run:

_ng serve_

The application will be available at:

http://localhost:4200/

You just have to enter any string in the search field and click search, and you should see a list with flights, 
if everything is up and running.
